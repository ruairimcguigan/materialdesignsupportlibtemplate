package demo.api.rest.com.api21template.view.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import demo.api.rest.com.api21template.R;
import demo.api.rest.com.api21template.model.adapters.DemoPagerAdapter;

import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.makeText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar mToolbar;
    FloatingActionButton mFloatingActionButton;
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    TabLayout mTabLayout;
    ViewPager mViewPager;
    DemoPagerAdapter demoPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpToolbar();
        initFloatingActionButton();
        initListeners();
        initDrawerLayout();
        initNavigationView();
        initTabLayout();
    }

    private void initTabLayout() {
        mTabLayout = (TabLayout)findViewById(R.id.tabLayout);
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        demoPagerAdapter = new DemoPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(demoPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

    }

    private void initDrawerLayout() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
    }

    /**
     *  sets a listener on the navigation view so that when a drawer menu item is selected,
     *  the menu item is set to checked (this will only affect the menu items marked as checkable),
     *  The drawer is closed and a Snackbar is shown displaying the title of the selected menu item.
     */
    private void initNavigationView() {

        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                mDrawerLayout.closeDrawers();
                makeSnackBar(mNavigationView, menuItem.getTitle().toString());
                return true;
            }
        });
    }

    private void initFloatingActionButton() {
        mFloatingActionButton = ( FloatingActionButton ) findViewById(R.id.fab);
        mFloatingActionButton.setOnClickListener(this);
    }

    private void setUpToolbar() {
        mToolbar = ( Toolbar ) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        if ( mToolbar != null ) {
            ActionBar actionBar = getSupportActionBar();
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        }
    }

    private void initListeners() {
        mFloatingActionButton.setOnClickListener(this);
    }


    //If needed - move tp Base Activity
    private void makeSnackBar(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).setAction("Action", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeText(MainActivity.this, "Snackbar Action", LENGTH_LONG).show();
            }
        }).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        int id = item.getItemId();

        switch(id){
            /**
             * This will bring the drawer onto the screen when the Home button
             * (the one with the hamburger menu icon) is tapped.
             */
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.action_settings:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch ( v.getId() ){
            case R.id.fab:
                makeSnackBar(v, "FAB Clicked");
        }
    }
}
