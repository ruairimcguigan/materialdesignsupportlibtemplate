package demo.api.rest.com.api21template.view.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import demo.api.rest.com.api21template.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class fragment_list_view extends Fragment {


    public fragment_list_view() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_list_view, container, false);
    }

}
