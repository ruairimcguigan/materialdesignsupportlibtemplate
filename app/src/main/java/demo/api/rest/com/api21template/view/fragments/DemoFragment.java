package demo.api.rest.com.api21template.view.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import demo.api.rest.com.api21template.R;
import demo.api.rest.com.api21template.model.adapters.DemoRecyclerAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class DemoFragment extends Fragment {

    RecyclerView mRecyclerView;

    private static final String TAB_POSITION = "Tab_Position";
    
    public DemoFragment() {
        // Required empty public constructor
    }

    public static DemoFragment newInstance(int tabPosition) {

        DemoFragment fragment = new DemoFragment();
        Bundle args = new Bundle();
        args.putInt(TAB_POSITION, tabPosition);
        fragment.setArguments(args);

        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle args = getArguments();
        int tabPosition = args.getInt(TAB_POSITION);

        ArrayList<String> items = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            items.add("Tab #" + tabPosition + " item #" + i);
        }

        View v = inflater.inflate(R.layout.fragment_fragment_list_view, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.recyclerview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(new DemoRecyclerAdapter(items));

        return v;
    }

}
