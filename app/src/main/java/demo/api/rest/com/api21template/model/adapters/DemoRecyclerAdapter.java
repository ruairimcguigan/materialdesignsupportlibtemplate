package demo.api.rest.com.api21template.model.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import demo.api.rest.com.api21template.R;
import demo.api.rest.com.api21template.view.activities.SecondActivity;

/**
 * Created by rmcguigan on 24/11/15.
 */
public class DemoRecyclerAdapter extends RecyclerView.Adapter<DemoRecyclerAdapter.Viewholder> {

    List<String> mItems;

    public DemoRecyclerAdapter(List<String> items){
        mItems = items;
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_row,
                viewGroup, false);

        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, int position) {

        String item = mItems.get(position);
        holder.text.setText(item);

        holder.text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                context.startActivity(new Intent(context, SecondActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView text;

        public Viewholder(View itemView) {
            super(itemView);

            text = (TextView) itemView.findViewById(R.id.list_item);
        }
    }
}
